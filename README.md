# Proyecto Pin 1

Este proyecto consiste en un pipeline en Jenkins para implementar un CI/CD utilizando Docker. El pipeline se encarga de construir una aplicación "Hola Mundo" en React, crear un contenedor Docker, y luego subir la imagen Docker resultante a Docker Hub.

## Configuración del Proyecto

### Requisitos Previos
- Jenkins instalado y configurado.
- GitLab configurado con un repositorio para el proyecto.
- Cuenta en Docker Hub.

### Conexión Jenkins-GitLab
Para conectar Jenkins y GitLab:
1. Generar un API token en GitLab.
2. Configurar Jenkins para que se active cada vez que se realiza un push al repositorio de GitLab.

## Estructura del Proyecto
El repositorio tiene la siguiente estructura:
- `src/`: Contiene el código fuente de la aplicación "Hola Mundo" en React.
- `Dockerfile`: Archivo de configuración para construir el contenedor Docker.

## Pasos del Pipeline

### 1. Construcción de la Imagen
Este paso construye la imagen Docker a partir del código fuente de la aplicación React.

// Paso 1: Construcción de la Imagen

~~~
dockerImage = docker.build registry + ":$BUILD_NUMBER"
~~~

### 2. Ejecución del Contenedor
Este paso ejecuta el contenedor Docker utilizando la imagen construida.

// Paso 2: Ejecución del Contenedor

~~~
sh "docker rm -f hola-mundo-jenkins"
sh "docker run -d -p 3000:80 --name hola-mundo-jenkins $registry:$BUILD_NUMBER"
sh "docker ps -f name=hola-mundo-jenkins"
~~~

### 3. Subida de la Imagen a DockerHub
Este paso sube la imagen Docker resultante a Docker Hub.

// Paso 3: Subida de la Imagen a DockerHub

~~~
docker.withRegistry("",registryCredentials){
    dockerImage.push()
}
~~~

### 4. Escaneo de Vulnerabilidades
Este paso escanea la imagen Docker en busca de vulnerabilidades utilizando Trivy.

// Paso 4: Escaneo de Vulnerabilidades

~~~
sh "docker run --name trivy -v /var/run/docker.sock:/var/run/docker.sock aquasec/trivy image --severity=CRITICAL --no-progress $registry:$BUILD_NUMBER --scanners vuln"
~~~


### 5. Limpieza del Entorno
Este paso limpia el entorno eliminando los contenedores utilizados para el escaneo de vulnerabilidades.

// Paso 5: Limpieza del Entorno

~~~
sh "docker rm trivy"
~~~

## Uso del Pipeline
Para ejecutar el pipeline en Jenkins, simplemente realiza un push al repositorio en GitLab. El pipeline se activará automáticamente y llevará a cabo los pasos descritos anteriormente.

## Notas Adicionales
Asegúrate de tener configuradas las credenciales de Docker Hub en Jenkins.
El pipeline está configurado para ejecutarse en un tiempo máximo de 30 minutos.