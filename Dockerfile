FROM node:14 AS builder

WORKDIR /app

COPY ./app/package.json ./

RUN npm install 

COPY ./app/ ./

RUN npm run build


FROM nginx:latest

WORKDIR /usr/share/nginx/html

RUN rm -rf ./*

COPY --from=builder /app/build .

ENTRYPOINT ["nginx", "-g", "daemon off;"]

