import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Presentacion GRUPO 9 - PRUEBA!
        </p>
        <a
          className="App-link"
          href="https://jenkins.feversani.ar"
          target="_blank"
          rel="noopener noreferrer"
        >
          Acceso a Jenkins
        </a>
      </header>
      <body>
        {window.location.hostname}
      </body>
    </div>
  );
}

export default App;
